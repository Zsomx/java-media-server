package hu.codingraven.jms.rest.response.json;

public class SongListJSON {
	String[] songs;

	public String[] getSongs() {
		return songs;
	}

	public void setSongs(String[] songs) {
		this.songs = songs;
	}
	
}
