package hu.codingraven.jms.rest.restcalls;
import javax.ws.rs.GET;
import hu.codingraven.jms.inner.files.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import hu.codingraven.jms.rest.response.json.SongListJSON;
@Path("/music")
public class Music {
	private static Thread t = null;
	@GET
	@Path("/getlist")
	@Produces("application/json")
	public SongListJSON getList(){
		SongListJSON songs = new SongListJSON();
		String[] songsTemp = FileList.getFileList();
		songs.setSongs(songsTemp);
		return songs;
	}
	@GET
	@Path("/play/{song}")
	public Response playSong(@PathParam("song") String song){
		SongPlayer.stop();
		SongPlayer.setFileName(song);
		if(t == null){
			t = new Thread(new SongPlayer());
		} else {
			threadKill(t);
			t = null;
			t = new Thread(new SongPlayer());
		}
		t.start();
		return Response.status(200).entity("Playing " + song + "...").build();
	}
	@GET
	@Path("/stop")
	public Response stopSong(){
		SongPlayer.stop();
		threadKill(t);
		t = null;
		return Response.status(200).entity("Stopped...").build();
	}
	
	private static void threadKill(Thread thread){
		try{
			thread.join();
			System.out.println("SongPlayer thread alive: "+ t.isAlive() + "\nSongPlayer thread state: " + t.getState());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
