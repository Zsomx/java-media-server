package hu.codingraven.jms.inner.files;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.*;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
public class SongPlayer implements Runnable{
	private static Player player = null;
	private static String path = "c:/Music/";
	private static String fileName = "";
	public static void setFileName(String name){
		fileName = name;
	}
	public static void play(){
		try{
			FileInputStream fis = new FileInputStream(path +fileName);
			BufferedInputStream bis = new BufferedInputStream(fis);
			player = new Player(bis);
			player.play();
		}  catch(FileNotFoundException e)
	    {
	        e.printStackTrace();
	    }
	    catch (JavaLayerException e)
	    {
	        e.printStackTrace();
	    }
	    catch (Exception e)
	    {
	        e.printStackTrace();
	    }
	    catch(ExceptionInInitializerError e)
	    {
	        e.printStackTrace();
	    }
	}
	public static void stop(){
		playerCheck();
	}
	
	public static void playerCheck(){
		if(player != null){
			if(!player.isComplete()){
				player.close();
			}
		}
	}
	@Override
	public void run() {
		play();
	}
}
