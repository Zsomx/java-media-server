package hu.codingraven.jms.inner.files;
import java.io.File;
public class FileList {
	public static String[] getFileList(){
		File f = null;
	    String[] paths;
	            
	      try{      
	         // create new file
	         f = new File("c:/Music");
	                                 
	         // array of files and directory
	         paths = f.list();
	         
	      }catch(Exception e){
	    	  paths = new String[1];
	    	  paths[0] = "None";
	      }
	      return paths;
	}
}
